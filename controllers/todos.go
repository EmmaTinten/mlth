package controllers

import (
	"net/http"
	"time"

	"codeberg.org/EmmaTinten/mlth/models"
	"codeberg.org/EmmaTinten/mlth/utils/token"
	"github.com/gin-gonic/gin"
)

type CreateTodoInput struct {
	Title       string    `json:"title" binding:"required"`
	Description string    `json:"description"`
	Due         time.Time `json:"due_date"`
}

type UpdateTodoInput struct {
	Title       string    `json:"title"`
	Description string    `json:"description"`
	Due         time.Time `json:"due_date"`
	UpdatedAt   time.Time
	// FIXME user und state
}

// find all todos
func FindTodos(c *gin.Context) {
	var todos []models.Todo

	userid, err := token.ExtractTokenID(c)
	if err != nil {
		return
	}
	// FIXME evtl. via Token Role den admins alle zurückgeben
	models.DB.Find(&todos, "user_id = ?", userid)

	c.JSON(http.StatusOK, gin.H{"data": todos})
}

func FindTodo(c *gin.Context) {
	var todo models.Todo

	userid, err := token.ExtractTokenID(c)
	if err != nil {
		return
	}
	// FIXME evtl. via Token Role den admins alle zurückgeben

	if err := models.DB.Where("id = ? AND user_id = ?", c.Param("id"), userid).First(&todo).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found"})
		return
	}

	c.JSON(http.StatusOK, gin.H{"data": todo})
}

func CreateTodo(c *gin.Context) {
	var input CreateTodoInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	userid, err := token.ExtractTokenID(c)
	if err != nil {
		return
	}
	// FIXME evtl. via Token Role den admins alle zurückgeben

	todo := models.Todo{
		Title:       input.Title,
		Description: input.Description,
		Due:         input.Due,
		UserID:      userid,
	}
	models.DB.Create(&todo)

	c.JSON(http.StatusOK, gin.H{"data": todo})
}

func UpdateTodo(c *gin.Context) {
	var todo models.Todo

	userid, err := token.ExtractTokenID(c)
	if err != nil {
		return
	}
	// FIXME evtl. via Token Role den admins alle zurückgeben

	if err := models.DB.Where("id = ? AND user_id = ?", c.Param("id"), userid).First(&todo).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	var input UpdateTodoInput
	if err := c.ShouldBindJSON(&input); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}

	updateTodo := models.Todo{
		Title:       input.Title,
		Description: input.Description,
		Due:         input.Due,
		// FIXME: User und State
	}

	models.DB.Model(&todo).Updates(&updateTodo)

	c.JSON(http.StatusOK, gin.H{"data": todo})
}

func DeleteTodo(c *gin.Context) {
	var todo models.Todo

	userid, err := token.ExtractTokenID(c)
	if err != nil {
		return
	}
	// FIXME evtl. via Token Role den admins alle zurückgeben

	if err := models.DB.Where("id = ? AND user_id = ?", c.Param("id"), userid).First(&todo).Error; err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": "Record not found!"})
		return
	}

	models.DB.Delete(&todo)

	c.JSON(http.StatusOK, gin.H{"data": true})
}
