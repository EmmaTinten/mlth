package models

import (
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

var DB *gorm.DB

func ConnectDabase() {
	database, err := gorm.Open(sqlite.Open("mlth.db"), &gorm.Config{})

	if err != nil {
		panic("Failed to connect to database!")
	}

	database.AutoMigrate(&Todo{})
	database.AutoMigrate(&User{})
	database.AutoMigrate(&State{})

	DB = database
}
