package main

import (
	"github.com/gin-gonic/gin"

	"codeberg.org/EmmaTinten/mlth/controllers"
	"codeberg.org/EmmaTinten/mlth/middlewares"
	"codeberg.org/EmmaTinten/mlth/models"
)

func main() {
	r := gin.Default()

	models.ConnectDabase()

	public := r.Group("/api/auth")

	public.POST("/register/", controllers.RegisterUser)

	public.POST("/login/", controllers.Login)

	protected := r.Group("/api")
	protected.Use(middlewares.JwtAuthMiddleware())
	protected.GET("/todo/", controllers.FindTodos)
	protected.POST("/todo/", controllers.CreateTodo)

	protected.GET("/todo/:id/", controllers.FindTodo)
	protected.PATCH("/todo/:id/", controllers.UpdateTodo)
	protected.DELETE("/todo/:id/", controllers.DeleteTodo)

	r.Run()

}
